
package transportadora;

public class Moto extends Veiculos {
    
    public Moto(){
        this.tipo = "Moto";
        combustivel = new Combustivel[2];
        combustivel[0] = new Combustivel("Gasolina", 4.449, 50, 0.3);
        combustivel[1] = new Combustivel("Alcool", 3.499, 43, 0.4);
        this.carga_maxima = 50;
        this.velocidade_maxima = 110;
    }
    
}
