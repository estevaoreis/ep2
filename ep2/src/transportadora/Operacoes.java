package transportadora;

public class Operacoes {

    private Transportadora transportadora;
    private int quantidade_veiculos[] = new int[4];
    private Carreta carreta_aux = new Carreta();
    private Van van_aux = new Van();
    private Carro carro_aux = new Carro();
    private Moto moto_aux = new Moto();
    private double custos_veiculos[]= new double[6];
    private double velocidades[] = new double[4];
    

    Operacoes(Carreta[] carretas, Carro[] carros, Van[] vans, Moto[] motos, double margem_lucro) {
        this.transportadora = new Transportadora();
        transportadora.setCarretas(carretas);
        transportadora.setVans(vans);
        transportadora.setMotos(motos);
        transportadora.setMargem_lucro(margem_lucro);

    }

    public double getCusto_veiculo(int identificador ) {
        return custos_veiculos[identificador];
    }

    public Transportadora getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(Transportadora transportadora) {
        this.transportadora = transportadora;
    }

    public int[] getQuantidade_veiculos() {
        return quantidade_veiculos;
    }

    public String Mensagens_Erros(int indice) {
        if (indice == 0) {
            return "Não há veiculos disponiveis.";
        } else if (indice == 1) {
            return "Nenhum veiculo disponivel é capaz de transportar tal carga";
        } else if (indice == 2) {
            return "Nenhum veiculo disponivel é capaz de transportar nesse periodo";
        } else if (indice == 3) {
            return "Não há veiculos disponiveis.";
        } else if (indice == 4) {
            return "problema na funcao calculaMenor_custo.";
        } else {
            return "";
        }
    }

    public int[] contaVeiculos() {
        for (Carreta carreta : transportadora.getCarretas()) {
            if (carreta.isDisponivel() == true) {
                this.quantidade_veiculos[0]++;
            }
        }
        for (Van van : transportadora.getVans()) {
            if (van.isDisponivel() == true) {
                this.quantidade_veiculos[1]++;
            }
        }
        for (Carro carro : transportadora.getCarros()) {
            if (carro.isDisponivel() == true) {
                this.quantidade_veiculos[2]++;
            }
        }
        for (Moto moto : transportadora.getMotos()) {
            if (moto.isDisponivel() == true) {
                this.quantidade_veiculos[3]++;
            }

        }
        return quantidade_veiculos;

    }

    public boolean verificaDisponibilidade_veiculos() {
        // Verifica se existe ao menos um veiculo disponivel
        boolean variavel_disponibilidade = false;
        for (int i = 0; i < 4; i++) {
            if (this.quantidade_veiculos[i] != 0) {
                variavel_disponibilidade = true;
                break;
            }
        }
        return variavel_disponibilidade;
    }

    public boolean verificaCapcidade_carregar(double carga) {
        //Verifica se há ao menos um veiculo disponivel capaz de transportar a carga.
        // Enquanto verifica ela filtra os possiveis veiculos, caso tenha algum que nao consiga
        // ela muda a quantidade do veiculo correspondente pra zero para facilitar 
        // as contas futuras. Pode ser interpretado como se, pelo fato de o veiculo nao ser
        //capaz de levar a carga , é com ose nao estivesse disponivel.
        // Essa verificacao é feita do menos capaz de suportar a carga ao mais capaz;
        boolean variavel_verificacao = false;
        if (quantidade_veiculos[3] > 0) {
            if (carga > moto_aux.getCarga_maxima()) {
                quantidade_veiculos[3] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[2] > 0) {
            if (carga > carro_aux.getCarga_maxima()) {
                quantidade_veiculos[2] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[1] > 0) {
            if (carga > van_aux.getCarga_maxima()) {
                quantidade_veiculos[1] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[0] > 0) {
            if (carga > carreta_aux.getCarga_maxima()) {
                quantidade_veiculos[0] = 0;
            } else {
                variavel_verificacao = true;
            }
        }

        return variavel_verificacao;
    }

    public boolean verificaCapacidade_entrega_no_tempo(double tempo, double distancia) {
        //Verifica se há ao menos um veiculo disponivel capaz de transportar a cara no tempo pedido.
        //Enquanto verifica ela filtra os possiveis veiculos capazes de levar 
        //no tempo, caso tenha algum que nao consiga ela muda a quantidade do 
        //veiculo correspondente pra zero para facilitar as contas futuras. 
        //Pode ser interpretado como se, pelo fato de o veiculo nao ser capaz de
        //levar a carga , é com ose nao estivesse disponivel.
        //Essa verificacao é feita do mais lento ao mais rapido;

        boolean variavel_verificacao = false;
        if (quantidade_veiculos[0] > 0) {
            if (tempo >= distancia / carreta_aux.getVelocidade_maxima()) {
                quantidade_veiculos[0] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[1] > 0) {
            if (tempo >= distancia / carreta_aux.getVelocidade_maxima()) {
                quantidade_veiculos[1] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[2] > 0) {
            if (tempo >= distancia / carreta_aux.getVelocidade_maxima()) {
                quantidade_veiculos[2] = 0;
            } else {
                variavel_verificacao = true;
            }
        }
        if (quantidade_veiculos[3] > 0) {
            if (tempo >= distancia / carreta_aux.getVelocidade_maxima()) {
                quantidade_veiculos[3] = 0;
            } else {
                variavel_verificacao = true;
            }
        }

        return variavel_verificacao;
    }
    public void calculaCustos(double carga, double distancia, double margem_lucro){
        //calcula o custo de cada veiculo e salva esses valores em um vetor com 6 posicoes
        //Isso é feito para evitar calcular novamente os valores em outras metodos 
        // nos quais é necessario saber esses valores com por exemplo nos metodos
        // verificaMenor_custo e verificaMaior_custo_beneficio
        
        if (quantidade_veiculos[0] > 0) {
            double rendimento_carreta = 0, custo_viagem_carreta;
            Combustivel[] combustivel_carreta;
            combustivel_carreta = carreta_aux.getCombustivel();
            combustivel_carreta[0].calculaRendimento(carga);
            rendimento_carreta = combustivel_carreta[0].getRendimento();
            custo_viagem_carreta = (distancia / rendimento_carreta) * combustivel_carreta[0].getPreco();
            this.custos_veiculos[0] = (custo_viagem_carreta * 100) / (100 - margem_lucro);
        }
        
        if (quantidade_veiculos[1] > 0) {
            double rendimento_van = 0, custo_viagem_van;
            Combustivel[] combustivel_van;
            combustivel_van = van_aux.getCombustivel();
            combustivel_van[0].calculaRendimento(carga);
            rendimento_van = combustivel_van[0].getRendimento();
            custo_viagem_van = (distancia / rendimento_van) * combustivel_van[0].getPreco();
            this.custos_veiculos[1] = (custo_viagem_van * 100) / (100 - margem_lucro);
        }

        if (quantidade_veiculos[2] > 0) {
            Combustivel[] combustivel_carro;
            double rendimento_gasolina, rendimento_alcool, custo_gasolina, custo_alcool;
            combustivel_carro = carro_aux.getCombustivel();

            combustivel_carro[0].calculaRendimento(carga);
            rendimento_gasolina = combustivel_carro[0].getRendimento();
            custo_gasolina = (distancia / rendimento_gasolina) * combustivel_carro[0].getPreco();
            this.custos_veiculos[2]  = (custo_gasolina * 100) / (100 - margem_lucro);

            combustivel_carro[1].calculaRendimento(carga);
            rendimento_alcool = combustivel_carro[1].getRendimento();
            custo_alcool = (distancia / rendimento_alcool) * combustivel_carro[1].getPreco();
            this.custos_veiculos[3]= (custo_alcool * 100) / (100 - margem_lucro);

        }
        if (quantidade_veiculos[3] > 0) {
            Combustivel[] combustivel_moto;
            double rendimento_gasolina, rendimento_alcool, custo_gasolina, custo_alcool;

            combustivel_moto = moto_aux.getCombustivel();

            combustivel_moto[0].calculaRendimento(carga);
            rendimento_gasolina = combustivel_moto[0].getRendimento();
            custo_gasolina = (distancia / rendimento_gasolina) * combustivel_moto[0].getPreco();
            this.custos_veiculos[4] = (custo_gasolina * 100) / (100 - margem_lucro);

            combustivel_moto[1].calculaRendimento(carga);
            rendimento_alcool = combustivel_moto[1].getRendimento();
            custo_alcool = (distancia / rendimento_alcool) * combustivel_moto[1].getPreco();
            this.custos_veiculos[5]= (custo_alcool * 100) / (100 - margem_lucro);
            
        }
    }
    
    public int verificaMenor_custo(){
        int identificador = 20;
        // 1 carreta, 2 van, 3 carro_gasolina , 4 carro_alcool, 5 moto_gasolina_ 6 moto_alcool
        double menor_custo = 100000000;
        for (int i = 0; i < 6; i++) {
            if (this.custos_veiculos[i] < menor_custo) {
                menor_custo = this.custos_veiculos[i];
                identificador = i+1;
            }
        }
        return identificador;
    }

    public int verificaMais_Rapido() {
        //Verifica qual é o veiculo disponivel com maior velocidade maxima.
        
        double maior_velocidade = 0;
        int identificadores[]= new int[]{1,2,3,4,};
        int identificador=0;
        
        if (quantidade_veiculos[0] > 0) {
            velocidades[0] = carreta_aux.getVelocidade_maxima();
        }
        if (quantidade_veiculos[1] > 0) {
            velocidades[1] = van_aux.getVelocidade_maxima();
        }
        if (quantidade_veiculos[2] > 0) {
            velocidades[2] = carro_aux.getVelocidade_maxima();
        }
        if (quantidade_veiculos[3] > 0) {
            velocidades[3] = moto_aux.getVelocidade_maxima();
            
        }
        for(int i=0; i<4; i++){
            if(velocidades[i]> maior_velocidade){
                maior_velocidade = velocidades[i];
                identificador = identificadores[i];
            }
        }
        return identificador;
    }
    
    public int verificaMaior_custo_beneficio(){
        double maior_custo_beneficio = 1000000;
        double custos_beneficios[]= new double[6];
        int identificador=0;
        
        if(quantidade_veiculos[0]>0){
            custos_beneficios[0] = custos_veiculos[0]/velocidades[0];
        }
        
        if(quantidade_veiculos[1]>0){
            custos_beneficios[1] = custos_veiculos[1]/velocidades[1];
        }
        
        if(quantidade_veiculos[2]>0){
            custos_beneficios[2] = custos_veiculos[2]/velocidades[2];
            custos_beneficios[3] = custos_veiculos[3]/velocidades[2];
        }
        
        if(quantidade_veiculos[3]>0){
            custos_beneficios[4] = custos_veiculos[4]/velocidades[3];
            custos_beneficios[5] = custos_veiculos[5]/velocidades[3];
        }
        for(int i=0; i<6; i++){
            if(custos_beneficios[i]<maior_custo_beneficio){
                maior_custo_beneficio = custos_beneficios[i];
                identificador = i+1;
            }
        }
        return identificador;
    }
}
