
package transportadora;

public class Combustivel {
    private String tipo;
    private double preco;
    private double rendimento;
    private double constante_reducao_rendimento;

    
    public Combustivel(){
    
    }
    public Combustivel(String tipo, double preco, double rendimento, double constante_reducao_rendimento){
        this.tipo = tipo;
        this.preco = preco;
        this.rendimento = rendimento;
        this.constante_reducao_rendimento = constante_reducao_rendimento;
    }
    //Métodos Get e Set
    public String getTipo() {
        return tipo;
    }

    public double getPreco() {
        return preco;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public double getConstante_reducao_rendimento() {
        return constante_reducao_rendimento;
    }
    
    public void calculaRendimento(double carga){
     this.rendimento -= carga* this.constante_reducao_rendimento;
    }
    
}
