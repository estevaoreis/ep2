package transportadora;

public class Carreta extends Veiculos {
    
    public Carreta(){
        this.tipo = "Carreta";
        combustivel  = new Combustivel[1];
        combustivel[0] = new Combustivel("Dielse", 3.869, 8, 0.0002);
        this.carga_maxima = 30000; // Em Kg
        this.velocidade_maxima = 60;// em Km    
        
    }

    public Combustivel[] getCombustivel() {
        return combustivel;
    }

   
}
