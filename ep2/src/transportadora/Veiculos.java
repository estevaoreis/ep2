package transportadora;

public abstract class Veiculos {
    protected String tipo;
    private double carga;
    protected float carga_maxima;
    protected float velocidade_maxima;
    private boolean disponivel;
    protected Combustivel [] combustivel;

    
    public Veiculos(){
        
    }
    // Métodos Get e Set

    public String getTipo() {
        return tipo;
    }
    
    public void setCarga(float carga) {
        this.carga = carga;
    }
    
    public double getCarga() {
        return carga;
    }
    
    public float getCarga_maxima() {
        return carga_maxima;
    }
    
    public float getVelocidade_maxima() {
        return velocidade_maxima;
    }

    public void setDisponivel(boolean disponivel) {
        this.disponivel = disponivel;
    }

    public boolean isDisponivel() {
        return disponivel;
    }

    public Combustivel[] getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(Combustivel[] combustivel) {
        this.combustivel = combustivel;
    }
    
}


